How to use images as Desktop Background:

1.Download an image into your home directory.

2.Open a window from Main Menu > System Tools (Preferences) > Tweaks > Appearance > Background > image. Or
 Open a window from Main Menu > System Tools > Budgie Control Center > Background > Add Picture...

3.Click Icon "image (none)"

4.Select the image from where you downloaded (ex. /home/demo).


See and download more images from
https://github.com/BuddiesOfBudgie/budgie-backgrounds

