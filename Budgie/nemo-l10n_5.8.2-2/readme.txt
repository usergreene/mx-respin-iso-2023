[How to add missing translation of your installed application]

These .mo files will add missing support for your locales in file manager. Add or overwrite the existing app's .mo file with updated .mo file from the commandline as below:

[Usage] Example:

root@mx1:/home/demo/# cp de.mo /usr/share/locale/de/LC_MESSAGES/nemo.mo

Note: "de" is a locale code for German language.

Source:
https://packages.debian.org/source/bookworm/cinnamon-translations

END.
